﻿using UnityEngine;


public class HpBar : CachedBehavior
{
    [SerializeField] private Transform maxValueTransform;
    [SerializeField] private Transform currentValueTransform;

    private Transform cameraTransform;
    private Vector3 startScale;
    private TimeCounter timer;
    private float timeForShow = 3.0f;

    private void Start()
    {
        cameraTransform = Camera.main.transform;
        startScale = currentValueTransform.localScale;
        timer = new TimeCounter( 0 );
    }

    private void Update()
    {
        CachedTransform.LookAt( CachedTransform.position + cameraTransform.rotation * Vector3.back, cameraTransform.rotation * Vector3.up );
    }

    public void SetPersant( float persants )
    {
        float previewPosition = currentValueTransform.localPosition.x - currentValueTransform.localScale.x / 2;
        currentValueTransform.localScale = new Vector3( startScale.x * persants, startScale.y, startScale.z );
        float curPos = currentValueTransform.localPosition.x - currentValueTransform.localScale.x / 2 - previewPosition;
        currentValueTransform.localPosition = new Vector3( currentValueTransform.localPosition.x + curPos, currentValueTransform.localPosition.y, currentValueTransform.localPosition.z );

        timer.Reset(timeForShow);
    }

    [ContextMenu( "Tesst 10" )]
    public void Test10()
    {
        SetPersant( 0.1f );
    }

    [ContextMenu( "Tesst 30" )]
    public void Test30()
    {
        SetPersant( 0.3f );
    }

    [ContextMenu( "Tesst 50" )]
    public void Test50()
    {
        SetPersant( 0.5f );
    }
}

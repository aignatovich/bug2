﻿using System.Collections.Generic;
using UnityEngine;

public class Definitions : IDefinitions
{
    public ApplicationSettingsDefinition ApplicationSettingsDefinitionDef { get; private set; }
    public MapDefinition MapDefinitionDef { get; private set; }
    public SwarmDefinition SwarmDefinitionDef { get; private set; }
    public ResourceFieldDefinition ResourceFieldDefinitionDef { get; private set; }

    public Dictionary<string, CreatureDefinition> CreaturesDictionary { get; private set; }
    public Dictionary<string, Dictionary<int, CreatureLevelDefinition>> CreatursLevelsDictionary { get; private set; }

    public Dictionary<string, ResourceDefinition> ResourcesDictionary { get; private set; }

    //потом надо както из джейсона справочники грузить
    public Definitions()
    {
        ApplicationSettingsDefinitionDef = new ApplicationSettingsDefinition( "Def_ApplicationSettings" );

        MapDefinitionDef = new MapDefinition( "Def_Map", 100, new Rect( 25, 25, 25, 25 ), new Rect( 40, 40, 15, 15 ) );

        SwarmDefinitionDef = new SwarmDefinition( "Def_Swarm", 4.0f, "Prefabs/Buildings/Swarm/Swarm" );

        Dictionary<string, float> rateResourcesDictionary = new Dictionary<string, float>();
        rateResourcesDictionary.Add( "Def_Resource", 1.0f );

        Dictionary<string, float> startResourcesDictionary = new Dictionary<string, float>();

        ResourceFieldDefinitionDef = new ResourceFieldDefinition( "Def_ResourceField", "Prefabs/MapItems/ResourceField/ResourcesField", startResourcesDictionary, rateResourcesDictionary, 12.0f );

        CreaturesDictionary = new Dictionary<string, CreatureDefinition>();
        CreaturesDictionary.Add( Constants.ScoutDefId, new CreatureDefinition( Constants.ScoutDefId, "scout", "Prefabs/Units/Scout/Scout", 10, 5.0f, 45 ) );
        CreaturesDictionary.Add( Constants.ForagerDefId, new ForagerDefinition( Constants.ForagerDefId, "forager", "Prefabs/Units/Forrager/Forrager", 10, 5.0f, 45, 1, 20.0f ) );

        CreatursLevelsDictionary = new Dictionary<string, Dictionary<int, CreatureLevelDefinition>>();
        var scoutLevels = new Dictionary<int, CreatureLevelDefinition>();
        scoutLevels.Add( 1, new CreatureLevelDefinition( 1, 10, 10, 10, "" ) );
        scoutLevels.Add( 2, new CreatureLevelDefinition( 2, 11, 11, 11, "" ) );
        scoutLevels.Add( 3, new CreatureLevelDefinition( 3, 12, 12, 12, "" ) );
        scoutLevels.Add( 4, new CreatureLevelDefinition( 4, 13, 13, 13, "" ) );
        scoutLevels.Add( 5, new CreatureLevelDefinition( 5, 14, 14, 14, "" ) );
        CreatursLevelsDictionary.Add( Constants.ScoutDefId, scoutLevels );

        var foragerLevels = new Dictionary<int, CreatureLevelDefinition>();
        foragerLevels.Add( 1, new CreatureLevelDefinition( 1, 10, 10, 10, "" ) );
        foragerLevels.Add( 2, new CreatureLevelDefinition( 2, 11, 11, 11, "" ) );
        foragerLevels.Add( 3, new CreatureLevelDefinition( 3, 12, 12, 12, "" ) );
        foragerLevels.Add( 4, new CreatureLevelDefinition( 4, 13, 13, 13, "" ) );
        foragerLevels.Add( 5, new CreatureLevelDefinition( 5, 14, 14, 14, "" ) );
        CreatursLevelsDictionary.Add( Constants.ForagerDefId, foragerLevels );

        ResourcesDictionary = new Dictionary<string, ResourceDefinition>();
        ResourcesDictionary.Add( "Def_Resource", new ResourceDefinition( "Def_Resource", "Prefabs/MapItems/ResourceItems/ResourceItem" ) );
    }
}

﻿using UnityEngine;
using Zenject;

public class GameManager : MonoInstaller
{
    [SerializeField] private GameObject resourceFieldPrefab;

    public override void InstallBindings()
    {
        Container.Bind<IGameModel>().ToSingle<GameModel>();
        Container.Bind<IDefinitions>().ToSingle<Definitions>();
        Container.Bind<IPool>().ToSingle<GlobalPool>();

        Container.Bind<ISwarmFactory>().ToSingle<SwarmFactory>();
        Container.Bind<ISwarmManager>().ToSingle<SwarmManager>();

        Container.Bind<UnitFactory>().ToSingle<UnitFactory>();

        Container.BindGameObjectFactory<ResourcessField.Factory>( resourceFieldPrefab );
    }

    public override void Start()
    {
        base.Start();
        InitGame();
    }

    private void InitGame()
    {
        ResourcessField.Factory resourceFieldFactory = Container.Resolve<ResourcessField.Factory>();
        resourceFieldFactory.Create();

        var swarmManager = Container.Resolve<ISwarmManager>();
        swarmManager.CreateSwarm( new Vector3( 20, 0, 21 ) );
    }
}

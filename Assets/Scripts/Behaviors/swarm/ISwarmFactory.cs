using UnityEngine;

public interface ISwarmFactory
{
    Swarm Create( Vector3 position );
}
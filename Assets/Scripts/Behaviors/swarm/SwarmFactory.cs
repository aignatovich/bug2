﻿using UnityEngine;
using Zenject;

public class SwarmFactory : ISwarmFactory
{
    private readonly IGameModel gameModel;
    private readonly IDefinitions definitions;
    private readonly GameObjectInstantiator instantiator;

    [Inject]
    public SwarmFactory( DiContainer container, IGameModel gameModel, IDefinitions definitions )
    {
        this.gameModel = gameModel;
        this.definitions = definitions;

        var swarmsRootTransformObject = new GameObject( "SwarmsRootTransform" );
        instantiator = new GameObjectInstantiator( container, swarmsRootTransformObject.transform );
    }

    public Swarm Create( Vector3 position )
    {
        var modelId = gameModel.GetNextModelId(Constants.CreatureModelPrefix);
        SwarmModel swarmModel = new SwarmModel( modelId, 0 );
        gameModel.SwarmDictionary.Add( modelId, swarmModel );

        var instatiatedObject = instantiator.Instantiate<Swarm>( Resources.Load( definitions.SwarmDefinitionDef.PrefabPath ) as GameObject );
        instatiatedObject.transform.position = position;
        var swarm = instatiatedObject.GetComponent<Swarm>();
        swarm.CachedTransform.position = position;
        swarm.Init( modelId );

        return swarm;
    }
}

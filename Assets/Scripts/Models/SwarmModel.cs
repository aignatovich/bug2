﻿public class SwarmModel : BaseModel
{
    public ExtendedInteger StoredResources { get; set; }

    public SwarmModel( string modelId, int storedResources ) : base( modelId )
    {
        StoredResources = storedResources;
    }
}

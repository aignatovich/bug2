﻿using System;
using UnityEngine;
using System.IO;
using NUnit.Framework;
using UnityEditor;

namespace UnityTest
{
    [TestFixture] [Category( "Global pool Tests" )] internal class GlobalPoolTest
    {
        [Test]
        [Category( "GlobalPool Spawn new GameObject" )]
        public void GlobalPoolTest_SpawnNewGameObject()
        {
            TestData testData = new TestData();

            Assert.IsNotNull( testData.SpawnedPrefab );
            Assert.AreEqual( testData.GlobalPool.CountAllStoredObjects(), 0 );
            Assert.AreEqual( testData.GlobalPool.CountStoredObjects( TestData.PrefabName ), 0 );

            testData.Dispose();
        }

        [Test]
        [Category( "GlobalPool Spawn pooled GameObject" )]
        public void GlobalPoolTest_SpawnPooledGameObject()
        {
            TestData testData = new TestData();

            testData.GlobalPool.Despawn( TestData.PrefabName, testData.SpawnedPrefab );

            Assert.AreEqual( testData.GlobalPool.CountAllStoredObjects(), 1 );
            Assert.AreEqual( testData.GlobalPool.CountStoredObjects( TestData.PrefabName ), 1 );

            testData.SpawnedPrefab = testData.GlobalPool.Spawn( TestData.PrefabName );

            Assert.IsNotNull( testData.SpawnedPrefab );
            Assert.AreEqual( testData.GlobalPool.CountAllStoredObjects(), 0 );
            Assert.AreEqual( testData.GlobalPool.CountStoredObjects( TestData.PrefabName ), 0 );

            testData.Dispose();
        }

        [Test]
        [Category( "GlobalPool Spawn new GameObject and recieve behavior script" )]
        public void GlobalPoolTest_SpawnNewGameObject_and_Recieve_behavior()
        {
            TestData testData = new TestData();

            AssetDatabase.SaveAssets();
            var spawnedBehavior = testData.GlobalPool.Spawn<MonoBehaviour>( TestData.PrefabName, Vector3.zero, Quaternion.identity );

            Assert.IsNotNull(spawnedBehavior);

            GameObject.DestroyImmediate( spawnedBehavior.gameObject );
            testData.Dispose();
        }

        [Test]
        [Category( "GlobalPool Spawn pooled GameObject and recieve behavior script" )]
        public void GlobalPoolTest_SpawnPooledGameObject_and_Recieve_behavior()
        {
            TestData testData = new TestData();

            var spawnedBehavior = testData.GlobalPool.Spawn<MonoBehaviour>( TestData.PrefabName, Vector3.zero, Quaternion.identity );
            testData.GlobalPool.Despawn( TestData.PrefabName, spawnedBehavior.gameObject );

            Assert.AreEqual( testData.GlobalPool.CountAllStoredObjects(), 1 );
            Assert.AreEqual( testData.GlobalPool.CountStoredObjects( TestData.PrefabName ), 1 );

            spawnedBehavior = testData.GlobalPool.Spawn<MonoBehaviour>( TestData.PrefabName, Vector3.zero, Quaternion.identity );

            Assert.IsNotNull( spawnedBehavior );
            Assert.AreEqual( testData.GlobalPool.CountAllStoredObjects(), 0 );
            Assert.AreEqual( testData.GlobalPool.CountStoredObjects( TestData.PrefabName ), 0 );

            GameObject.DestroyImmediate( spawnedBehavior.gameObject );
            testData.Dispose();
        }

        [Test]
        [Category( "GlobalPool Despawn GameObject" )]
        public void GlobalPoolTest_DespawnGameObject()
        {
            TestData testData = new TestData();

            Assert.AreEqual(testData.GlobalPool.CountAllStoredObjects(), 0);
            Assert.AreEqual(testData.GlobalPool.CountStoredObjects(TestData.PrefabName), 0);

            testData.GlobalPool.Despawn(TestData.PrefabName, testData.SpawnedPrefab);

            Assert.AreEqual(testData.GlobalPool.CountAllStoredObjects(), 1);
            Assert.AreEqual(testData.GlobalPool.CountStoredObjects(TestData.PrefabName), 1);

            testData.Dispose();
        }
    }

    internal class TestData : IDisposable
    {
        public const string PathToResourcess = "Assets/Resources/";
        public const string PrefabName = "TestPrefab";
        public const string PrefabExtension = ".prefab";

        public GlobalPool GlobalPool;
        public GameObject SpawnedPrefab;

        private string fullPathToPrefab;

        private GameObject gameObjectFoPrefab;

        public TestData()
        {
            fullPathToPrefab = Path.ChangeExtension( Path.Combine( PathToResourcess, PrefabName ), PrefabExtension );
            GlobalPool = new GlobalPool();

            gameObjectFoPrefab = new GameObject( "go" );
            gameObjectFoPrefab.AddComponent<MonoBehaviour>();
            PrefabUtility.CreatePrefab( fullPathToPrefab, gameObjectFoPrefab );
            AssetDatabase.SaveAssets();

            SpawnedPrefab = GlobalPool.Spawn( PrefabName );
        }

        public void Dispose()
        {
            GlobalPool.Dispose();
            GameObject.DestroyImmediate( gameObjectFoPrefab );
            AssetDatabase.DeleteAsset( fullPathToPrefab );
            GameObject.DestroyImmediate( SpawnedPrefab );
        }
    }
}

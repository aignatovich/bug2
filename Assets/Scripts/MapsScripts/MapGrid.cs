﻿using UnityEngine;

public class MapGrid 
{
	public CellType[,] cells { get; private set; }
	/// <summary>
	/// левый нижний угол грида
	/// </summary>
	Vector3 startPoint;

	public MapGrid( Vector3 centerPoint, int sizeOfGrid )
	{
		this.startPoint = centerPoint;
		cells = new CellType[sizeOfGrid, sizeOfGrid];
	}

	public Vector3 GetRealPositionOfCell( Vector2 cellPoint )
	{
		return new Vector3( startPoint.x + cellPoint.x * Constants.SizeOfCell, startPoint.y, startPoint.z + cellPoint.y * Constants.SizeOfCell );
	}

	public Vector2 GetGridPositionByRealPosition( Vector3 worldPosition )
	{
		return new Vector2( ( worldPosition.x - startPoint.x ) / Constants.SizeOfCell, ( worldPosition.z - startPoint.z ) / Constants.SizeOfCell ); 
	}
}

public enum CellType
{
	Available,
	UnAvailable
}

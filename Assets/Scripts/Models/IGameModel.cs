﻿public interface IGameModel
{
    DictionaryAdapter<CreatureModel> CreaturesDictionary { get; }
    DictionaryAdapter<SwarmModel> SwarmDictionary { get; }

    string GetNextModelId( string idPrefix );
}
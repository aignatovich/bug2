﻿using UnityEngine;
using System.Collections.Generic;
using Zenject;

public class Swarm : CachedBehavior
{
    public string ModelId { get; private set; }

    public Vector3 Position
    {
        get
        {
            return CachedTransform.position;
        }
    }

    [Inject] private IGameModel gameModel;
    [Inject] private UnitFactory unitFactory;

	private List<ScoutInfo> scoutedObjects;
    
    public void Init(  string modelId)
	{
	    ModelId = modelId;
		scoutedObjects = new List<ScoutInfo>();
		CreateScout();
	}

    public void CreateForager()
    {
        unitFactory.CreateCreature( Constants.ForagerDefId, ModelId, 0, transform.position );
    }

    public void CreateScout()
    {
        unitFactory.CreateCreature( Constants.ScoutDefId, ModelId, 0, transform.position );
    }

    public bool TryPutResources( int value )
    {
        gameModel.SwarmDictionary[ModelId].StoredResources += value;
        return true;
    }

	public void PutScoutInfo( List<ScoutInfo> newScoutedObjects )
	{
		scoutedObjects.AddRange( newScoutedObjects );
	    if ( scoutedObjects.Count > 0 )
	    {
	        CreateForager();
	    }
	}
}

﻿using UnityEngine;
using Zenject;

public class UnitFactory
{
    private readonly IGameModel gameModel;
    private readonly IDefinitions definitions;
    private readonly GameObjectInstantiator instantiator;

    [Inject]
    public UnitFactory( DiContainer container, IGameModel gameModel, IDefinitions definitions )
    {
        this.gameModel = gameModel;
        this.definitions = definitions;

        var creaturesRootTransformObject = new GameObject( "UnitRootTransform" );
        instantiator = new GameObjectInstantiator( container, creaturesRootTransformObject.transform );
    }

    public BaseCreatureBehavior CreateCreature( string creatureDefinitionId, string swarmModelId, int level, Vector3 position )
    {
        var modelId = gameModel.GetNextModelId( Constants.CreatureModelPrefix );
        var newModel = new CreatureModel( modelId, creatureDefinitionId, swarmModelId, level, position, 0, definitions.CreaturesDictionary[creatureDefinitionId].Hp );
        gameModel.CreaturesDictionary.Add( modelId, newModel );

        var instatiatedObject = instantiator.Instantiate( Resources.Load(  definitions.CreaturesDictionary[creatureDefinitionId].PassToPrefab ) as GameObject );
        instatiatedObject.transform.position = position;
        
        var newCreature = instatiatedObject.GetComponent<BaseCreatureBehavior>();
        newCreature.Init( modelId );

        return newCreature;
    }
}

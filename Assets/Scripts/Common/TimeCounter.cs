﻿public class TimeCounter
{
    public double Time { get; private set; }
    public double ElapsedTime { get; protected set; }

    public bool IsElapsed
    {
        get { return ElapsedTime <= 0.0f; }
    }

    public TimeCounter( double time )
    {
        Time = time;
        ElapsedTime = time;
    }

    public void ElapseAndReset( float deltaTime )
    {
        ElapsedTime -= deltaTime;

        if ( IsElapsed )
        {
            double error = ElapsedTime;
            ElapsedTime = Time - error;
        }
    }

    public void Elapse( float deltaTime )
    {
        if ( IsElapsed )
            return;

        ElapsedTime -= deltaTime;
    }

    public void Reset()
    {
        ElapsedTime = Time;
    }

    public void Reset( float newTime )
    {
        Time = newTime;
        Reset();
    }

    public void SetElapsedTimeByCounter( TimeCounter counter )
    {
        ElapsedTime = counter.ElapsedTime * Time / counter.Time;
    }

    public void ReverseElapsedTime()
    {
        ElapsedTime = Time - ElapsedTime;
    }
}
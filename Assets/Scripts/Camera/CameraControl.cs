﻿using UnityEngine;

public class CameraControl : MonoBehaviour, ICameraControl
{
    [Tooltip( "скорость движения камеры" )] [SerializeField] private float cameraSpeed = 0.2f;
    [Tooltip( "скорость скролла камеры" )] [SerializeField] private float scrollSpeed = 10.0f;

    private KeyCode upKey = KeyCode.W;
    private KeyCode dawnKey = KeyCode.S;
    private KeyCode leftKey = KeyCode.A;
    private KeyCode rightKey = KeyCode.D;

    private Vector3 moveStep;
    private float newCameraHeight;

    public void SetCameraPosition( Vector3 position )
    {
        transform.position = new Vector3( position.x, transform.position.y, position.z );
    }

    private void Update()
    {
        if ( Input.GetKeyDown( upKey ) )
        {
            moveStep += new Vector3( 0, 0, cameraSpeed );
        }

        if ( Input.GetKeyDown( dawnKey ) )
        {
            moveStep += new Vector3( 0, 0, -cameraSpeed );
        }

        if ( Input.GetKeyDown( leftKey ) )
        {
            moveStep += new Vector3( -cameraSpeed, 0, 0 );
        }

        if ( Input.GetKeyDown( rightKey ) )
        {
            moveStep += new Vector3( cameraSpeed, 0, 0 );
        }

        if ( Input.GetKeyUp( upKey ) )
        {
            moveStep += new Vector3( 0, 0, -cameraSpeed );
        }

        if ( Input.GetKeyUp( dawnKey ) )
        {
            moveStep += new Vector3( 0, 0, cameraSpeed );
        }

        if ( Input.GetKeyUp( leftKey ) )
        {
            moveStep += new Vector3( cameraSpeed, 0, 0 );
        }

        if ( Input.GetKeyUp( rightKey ) )
        {
            moveStep += new Vector3( -cameraSpeed, 0, 0 );
        }

        newCameraHeight = Input.GetAxis( "Mouse ScrollWheel" );
        moveStep.y = newCameraHeight * scrollSpeed;

        transform.Translate( moveStep );
    }
}

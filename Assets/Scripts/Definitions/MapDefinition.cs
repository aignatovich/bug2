﻿using UnityEngine;

public class MapDefinition : BaseDefinition
{
    public int SizeOfGrid { get; private set; }
    public Rect SwarmSpawnArea { get; private set; }
    public Rect ResourceFieldSpawnArea { get; private set; }

    public MapDefinition( string definitionId, int sizeOfGrid, Rect swarmSpawnArea, Rect resourceFieldSpawnArea ) : base( definitionId )
    {
        SizeOfGrid = sizeOfGrid;
        SwarmSpawnArea = swarmSpawnArea;
        ResourceFieldSpawnArea = resourceFieldSpawnArea;
    }
}

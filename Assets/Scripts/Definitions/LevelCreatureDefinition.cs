﻿public class CreatureLevelDefinition: BaseLevelDefinition
{
    public float Speed { get; private set; }
    public float Attack { get; private set; }
    public int Hp { get; private set; }
    public string VisualPrefabPath { get; private set; }

    public CreatureLevelDefinition( int level, float speed, float attack, int hp, string visualPrefabPath ) : base( level )
    {
        Speed = speed;
        Attack = attack;
        Hp = hp;
        VisualPrefabPath = visualPrefabPath;
    }
}

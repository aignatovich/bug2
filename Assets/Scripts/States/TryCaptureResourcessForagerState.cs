﻿using UnityEngine;

public class TryCaptureResourcessForagerState : BaseState<Forager>
{
    public TryCaptureResourcessForagerState( Forager forager ) : base( forager ) { }

    public override FSMResult Execute()
    {
        if ( TryGetResource() )
        {
            if (context.Collected >= Constants.ForagerResourcesCapacity)
            {
                nextState = FsmNames.ReturnResourcesToSwarmForagerState.ToString();
                return FSMResult.Finished;
            }

            nextState = FsmNames.SearchForResourcesForragerState.ToString();
            return FSMResult.Finished;
        }

        context.Step( Vector3.forward );
        return FSMResult.Continiue;
    }

    private bool TryGetResource()
    {
        RaycastHit hit = new RaycastHit();
        // linecast between points
        Debug.DrawLine( context.transform.position, (context.transform.position + 2 * context.transform.forward ), Color.red );
        if ( Physics.Linecast( context.transform.position, ( context.transform.position + 2 * context.transform.forward ) , out hit, 1 << LayerMask.NameToLayer( Layers.Resource.ToString() ) ) )
        {
            context.CollectResource( hit.collider.gameObject );
            return true;
        }

        return false;
    }
}

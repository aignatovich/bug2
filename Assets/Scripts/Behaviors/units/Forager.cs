﻿using UnityEngine;
using Zenject;

public class Forager : BaseCreatureBehavior
{
    [Inject] private ISwarmManager swarmManager;
    [Inject] private IGameModel gameModel;

    private int collectedResourcess;
    private bool isResourceFinding;

    public int Collected
    {
        get { return collectedResourcess; }
        set { collectedResourcess = value; }
    }

    protected override void InitStateMachine()
    {
        base.InitStateMachine();
        fsm.AddState( FsmNames.SearchForResourcesForragerState.ToString(), typeof ( SearchForResourcesForagerState ) );
        fsm.AddState( FsmNames.TryCaptureResourcessForagerState.ToString(), typeof ( TryCaptureResourcessForagerState ) );
        fsm.AddState( FsmNames.ReturnResourcesToSwarmForagerState.ToString(), typeof ( ReturnResourcesToSwarmForagerState ) );
        fsm.SetStartState( FsmNames.SearchForResourcesForragerState.ToString() );
    }

    public void CollectResource( GameObject resource )
    {
        Collected++;
        Destroy( resource );
    }

    public bool TryReturnResources()
    {
        if (swarmManager.Swarms[gameModel.CreaturesDictionary[ModelId].SwarmModelId].TryPutResources(Collected))
        {
            Collected = 0;
            return true;
        }
        return false;
    }
}
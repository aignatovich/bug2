﻿using UnityEngine;
using Zenject;

public class ReturnResourcesToSwarmForagerState : BaseState<Forager>
{
    [Inject] private ISwarmManager swarmManager;
    [Inject] private IGameModel gameModel;
    [Inject] private IDefinitions definitions;

    public ReturnResourcesToSwarmForagerState( Forager forager ) : base( forager )
    {
    }

    public override void Enter()
    {
        context.transform.LookAt(swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position);
    }

    public override FSMResult Execute()
    {
        context.transform.LookAt(swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position);
        context.Step( Vector3.forward );

        if ( Vector3.Distance( context.transform.position, swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position ) < definitions.SwarmDefinitionDef.Radius )
        {
            if ( context.TryReturnResources() )
            {
                nextState = FsmNames.SearchForResourcesForragerState.ToString();
                return FSMResult.Finished;
            }
        }

        return FSMResult.Continiue;
    }
}
﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SwarmManager : ISwarmManager
{
    [Inject] private ISwarmFactory swarmFactory;
    [Inject] private IGameModel gameModel;

    public Dictionary<string, Swarm> Swarms { get; private set; }

    [Inject]
    public SwarmManager( IGameModel gameModel, ISwarmFactory swarmFactory )
    {
        this.gameModel = gameModel;
        this.swarmFactory = swarmFactory;
        Swarms = new Dictionary<string, Swarm>();
        this.gameModel.SwarmDictionary.OnItemRemovedEvent += OnSwarmRemovedEvent;
    }

    private void OnSwarmRemovedEvent( object sender, DictionaryAdapter<SwarmModel>.ItemRemovedOnContextEventArgs<SwarmModel> arg )
    {
        Swarms.Remove( arg.Key );
    }

    public Swarm CreateSwarm( Vector3 position )
    {
        var newSwarm = swarmFactory.Create( position );
        Swarms.Add( newSwarm.ModelId, newSwarm );
        return newSwarm;
    }
}
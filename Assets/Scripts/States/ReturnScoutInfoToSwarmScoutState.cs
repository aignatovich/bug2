﻿using UnityEngine;
using Zenject;

public class ReturnScoutInfoToSwarmScoutState : BaseState<Scout>
{
    [Inject] private ISwarmManager swarmManager;
    [Inject] private IGameModel gameModel;
    [Inject] private IDefinitions definitions;

    public ReturnScoutInfoToSwarmScoutState( Scout scout ) : base( scout )
    {
    }

    public override void Enter()
    {
        context.transform.LookAt(swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position);
    }

    public override FSMResult Execute()
    {
        context.transform.LookAt(swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position);
        context.Step( Vector3.forward );

        if (Vector3.Distance(context.transform.position, swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position) < definitions.SwarmDefinitionDef.Radius)
        {
            context.ReturnScoutInfo();
            nextState = FsmNames.ScoutTerritoryScoutState.ToString();
            return FSMResult.Finished;
        }

        return FSMResult.Continiue;
    }
}
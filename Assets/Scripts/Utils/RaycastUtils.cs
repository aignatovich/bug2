﻿using UnityEngine;


public static class RaycastUtils  
{
	public static bool RaycastByAngle( Transform startTransform, float distance, float angle, int layerMask, out GameObject findedObject )
	{
		Vector3 targetPos = startTransform.forward * angle;
		
		int startAngle = (int) ( -angle * 0.5f ); // half the angle to the Left of the forward
		int finishAngle = (int) ( angle * 0.5f ); // half the angle to the Right of the forward

		RaycastHit hit = new RaycastHit();

		// step through and find each target point
		for ( int i = startAngle; i < finishAngle; ++i ) // Angle from forward
		{
			targetPos.x = startTransform.transform.position.x + Mathf.Sin( ( i + startTransform.transform.rotation.eulerAngles.y ) * Mathf.Deg2Rad ) * angle;
			targetPos.z = startTransform.transform.position.z + Mathf.Cos( ( i + startTransform.transform.rotation.eulerAngles.y ) * Mathf.Deg2Rad ) * angle;
			
			// linecast between points
			if ( Physics.Linecast( startTransform.position, targetPos, out hit, layerMask ) )
			{
				findedObject = hit.collider.gameObject;
				return true;
			}
			
			// to show ray just for testing
			Debug.DrawLine( startTransform.position, targetPos, Color.green );
		}

		findedObject = null;
		return false;
	}
}

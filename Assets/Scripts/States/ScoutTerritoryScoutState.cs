using UnityEngine;
using Zenject;

public class ScoutTerritoryScoutState : BaseState<Scout>
{
    [Inject] private ISwarmManager swarmManager;
    [Inject] private IGameModel gameModel;
    [Inject] private IDefinitions definitions;

    public ScoutTerritoryScoutState( Scout scout ) : base( scout )
    {
    }

    public override void Enter()
    {
        context.CalcNextPositionForScout();
    }

    public override FSMResult Execute()
    {
        context.transform.LookAt( context.targetPositionForScout );
        context.Step( Vector3.forward );

        if ( Vector3.Distance( context.targetPositionForScout, context.transform.position ) < 2 )
        {
            context.CalcNextPositionForScout();
        }

        GameObject findedObject;

        if ( RaycastUtils.RaycastByAngle( context.transform, definitions.CreaturesDictionary[gameModel.CreaturesDictionary[context.ModelId].DefinitionId].VisionDistance, definitions.CreaturesDictionary[gameModel.CreaturesDictionary[context.ModelId].DefinitionId].VisionAngle, 1 << LayerMask.NameToLayer( Layers.Resource.ToString() ), out findedObject ) )
        {
            if ( findedObject.layer == LayerMask.NameToLayer( Layers.Resource.ToString() ) )
            {
                context.AddNewScoutedPosition( new ScoutInfo( TypeOfScoutingObject.Resource, findedObject.transform.position ) );
            }

            if ( context.IsScoutTargetReached() )
            {
                nextState = FsmNames.ReturnScoutInfoToSwarmScoutState.ToString();
                return FSMResult.Finished;
            }
        }

        return FSMResult.Continiue;
    }
}

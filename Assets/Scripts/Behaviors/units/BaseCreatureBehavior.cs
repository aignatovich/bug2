﻿using UnityEngine;

public class BaseCreatureBehavior : BaseFsmBehavior 
{
    public string ModelId { get; private set; }

	[SerializeField] protected float speed;

    #region Pivots
    [SerializeField] protected GameObject hpBarPivot;
    #endregion

    protected HpBar hpBar;
    
    public virtual void Init( string modelId )
	{
        ModelId = modelId;
        BuildGameObject();
	}

    private void BuildGameObject()
    {
        hpBar = Utils.InstatiateObjectByPass<HpBar>( Constants.PathToHpBarPrefab, hpBarPivot );
    }

	public void Step( Vector3 direction )
	{
		transform.Translate( direction.normalized * speed );
	}
}

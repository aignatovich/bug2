﻿public class SwarmDefinition : BaseDefinition 
{
    public float Radius { get; private set; }
    public string PrefabPath { get; private set; }

    public SwarmDefinition( string definitionId, float radius, string prefabPath ) : base( definitionId )
    {
        Radius = radius;
        PrefabPath = prefabPath;
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public interface ISwarmManager
{
    Swarm CreateSwarm( Vector3 position );
    Dictionary<string, Swarm> Swarms { get; }
}
﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    private static object _lock = new object();

    public static T Instance
    {
        get
        {
            lock ( _lock )
            {
                if ( instance == null )
                {
                    instance = (T) FindObjectOfType<T>();
                    if ( FindObjectsOfType<T>().Length > 1 )
                    {
                        return instance;
                    }

                    if ( instance == null )
                    {
                        GameObject singleton = new GameObject();
                        instance = singleton.AddComponent<T>();
                    }
                }

                return instance;
            }
        }
    }
}


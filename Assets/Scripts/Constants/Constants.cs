﻿public  struct Constants
{
    public const float AngleForTurn = 30.0f;
    public const int ForagerResourcesCapacity = 1;
	public const float SizeOfCell = 1;
    public const int MinLevel = 1;

    public const string PathToHpBarPrefab = "Prefabs/Common/HpBar";

    public const string ForagerDefId = "Def_Forager";
    public const string ScoutDefId = "Def_Scout";

    public const string MapDefId = "Def_Map";

    public const string CreatureModelPrefix = "Creature";
    public const string SwarmModelPrefix = "Swarm";

    //Log formats
    public const string ScoutInfoToString = "Type of scouting object: {0} in position: {1}";
}

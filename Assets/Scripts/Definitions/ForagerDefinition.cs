﻿public class ForagerDefinition : CreatureDefinition
{
    public int ResourceCapacity { get; private set; }
    public float MaxDistanceFromSwarm { get; private set; }

    public ForagerDefinition( string definitionId, string name, string passToPrefab, int hp, float visionDistance, int visionAngle, int resourceCapacity, float maxDistanceFromSwarm ) : base( definitionId, name, passToPrefab, hp, visionDistance, visionAngle )
    {
        ResourceCapacity = resourceCapacity;
        MaxDistanceFromSwarm = maxDistanceFromSwarm;
    }
}

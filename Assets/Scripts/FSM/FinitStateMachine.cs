﻿using System;
using System.Collections.Generic;
using Zenject;

public class FinitStateMachine<TContext>
{
    private Dictionary<string, Type> states = new Dictionary<string, Type>();
    private IState currentState;

    private TContext context;
    private readonly DiContainer container;

    public FinitStateMachine( TContext context, DiContainer container )
    {
        this.context = context;
        this.container = container;
    }

    public void AddState( string nameOfState, Type newState )
    {
        states.Add( nameOfState, newState );
    }

    public void SetStartState( string nameOfStartState )
    {
        currentState = InstatiateState( states[nameOfStartState] );
        currentState.Enter();
    }

    public void Execute()
    {
        if ( currentState.Execute() == FSMResult.Finished )
        {
            currentState.Exit();
            currentState = InstatiateState( states[currentState.nextState] );
            currentState.Enter();
        }
    }

    private IState InstatiateState( Type typeOfState )
    {
        var state = container.Instantiate( typeOfState, new object[] {context} );
        return (IState) ( state );
    }
}

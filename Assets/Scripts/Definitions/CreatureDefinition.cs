﻿public class CreatureDefinition : BaseDefinition
{
    public string Name { get; private set; }
    public string PassToPrefab { get; private set; }
    public int Hp { get; private set; }
    public float VisionDistance { get; private set; }
    public int VisionAngle { get; private set; }

    public CreatureDefinition( string definitionId, string name, string passToPrefab, int hp, float visionDistance, int visionAngle ) : base( definitionId )
    {
        Name = name;
        PassToPrefab = passToPrefab;
        Hp = hp;
        VisionDistance = visionDistance;
        VisionAngle = visionAngle;
    }
}

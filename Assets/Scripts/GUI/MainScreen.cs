﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainScreen : Singleton<MainScreen>
{
    //items
    [SerializeField] private Text resourcessLabel;

    //prefabs
    [SerializeField] private GameObject shopDialogPrefab;

    private string resourceLabelText = "Resourcess {0}";
    private Shop shop;

    private void Start()
    {
//        GameManager.Instance.GameModel.SwarmDictionary.dictionary.First().Value.StoredResources.Changed += OnChangeResourcess;
    }

    private void OnChangeResourcess( ExtendedInteger.ExtendedIntegerEventArgs arg )
    {
        resourcessLabel.text = string.Format( resourceLabelText, arg.NewValue );
    }

    private void OpenShop()
    {
        shop = ( Instantiate( shopDialogPrefab ) as GameObject ).GetComponent<Shop>();
        shop.Init();
    }
}

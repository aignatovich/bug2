﻿using UnityEngine;

public interface ICameraControl
{
    void SetCameraPosition( Vector3 position );
}
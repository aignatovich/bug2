﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public event EventHandler<ShopItemEventArgs> OnItemSelected;

    [SerializeField] private Button button;
    [SerializeField] private Text textLabel;

    private string pathToPrefab;

    public void Init( string itemName, string pathToPrefab )
    {
        textLabel.text = itemName;
        this.pathToPrefab = pathToPrefab;
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        if ( OnItemSelected != null )
        {
            OnItemSelected( this, new ShopItemEventArgs( pathToPrefab ) );
        }
    }

    public class ShopItemEventArgs: EventArgs
    {
        public string PathToPrefab;

        public ShopItemEventArgs( string pathToPrefab )
        {
            PathToPrefab = pathToPrefab;
        }
    }
}

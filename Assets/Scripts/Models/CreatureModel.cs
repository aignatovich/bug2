﻿using UnityEngine;

public class CreatureModel : BaseModel
{
    public string DefinitionId { get; private set; }
    public string SwarmModelId { get; private set; }
    public ExtendedInteger Hp { get; set; }
    public Vector3 Position { get; set; }
    public float Rotation { get; set; }
    public int Level { get; set; }

    public CreatureModel( string modelId, string definitionId, string swarmModelId, int level, Vector3 position, float rotation, int hp ) : base( modelId )
    {
        DefinitionId = definitionId;
        SwarmModelId = swarmModelId;
        Hp = hp;
        Position = position;
        Rotation = rotation;
        Level = level;
    }
}

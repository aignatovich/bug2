﻿public class GameModel : IGameModel
{
    public DictionaryAdapter<CreatureModel> CreaturesDictionary { get; private set; }
    public DictionaryAdapter<SwarmModel> SwarmDictionary { get; private set; }

    //начально значение надо переделать
    private int lastModelId = 1000;

    ///этот метод загружает модель, пока она создаётся, но потом должна грузиться из файлика
    public GameModel()
    {
        CreaturesDictionary = new DictionaryAdapter<CreatureModel>();
        SwarmDictionary = new DictionaryAdapter<SwarmModel>();
    }

    public string GetNextModelId( string idPrefix )
    {
        return idPrefix + ++lastModelId;
    }
}

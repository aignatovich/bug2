﻿using UnityEngine;
using System.Collections.Generic;
using Zenject;

public class Scout : BaseCreatureBehavior 
{
    [Inject] private ISwarmManager swarmManager;
    [Inject] private IGameModel gameModel;

	public Vector3 targetPositionForScout{private set; get;}
	private int maxScoutingObjects;

	private List<ScoutInfo> scoutedObjects;
	
	public override void Init( string modelId )
	{
	    base.Init( modelId );
		scoutedObjects = new List<ScoutInfo>();
	}

	public void SetScoutParametres( int maxScoutingObjects )
	{
		this.maxScoutingObjects = maxScoutingObjects;
	}

	protected override void InitStateMachine()
	{
		base.InitStateMachine();
		fsm.AddState( FsmNames.ScoutTerritoryScoutState.ToString(), typeof ( ScoutTerritoryScoutState ) );
		fsm.AddState( FsmNames.ReturnScoutInfoToSwarmScoutState.ToString(), typeof ( ReturnScoutInfoToSwarmScoutState ) );
		
		fsm.SetStartState( FsmNames.ScoutTerritoryScoutState.ToString() );
	}

	public void CalcNextPositionForScout()
	{
        //TODO убрать константу
		targetPositionForScout = new Vector3( Random.Range( 0, 50 ), 0, Random.Range( 0, 50) );
	}

	public bool AddNewScoutedPosition( ScoutInfo newScoutedInfo )
	{
		if ( !scoutedObjects.Exists( scoutedInfo => scoutedInfo == newScoutedInfo ) )
		{
			scoutedObjects.Add( newScoutedInfo );
			return true;
		}

		return false;
	}

	public bool IsScoutTargetReached()
	{
		return scoutedObjects.Count >= maxScoutingObjects;
	}

    public void ReturnScoutInfo()
    {
        swarmManager.Swarms[gameModel.CreaturesDictionary[ModelId].SwarmModelId].PutScoutInfo( scoutedObjects );
        scoutedObjects.Clear();
    }
}

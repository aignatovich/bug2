﻿using UnityEngine;

public class CachedBehavior : MonoBehaviour 
{
    public Transform CachedTransform { get; private set; }

    void Awake ()
    {
        CachedTransform = transform;
    }
}

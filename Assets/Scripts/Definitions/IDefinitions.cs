﻿using System.Collections.Generic;

public interface IDefinitions
{
    ApplicationSettingsDefinition ApplicationSettingsDefinitionDef { get; }
    MapDefinition MapDefinitionDef { get; }
    SwarmDefinition SwarmDefinitionDef { get; }
    ResourceFieldDefinition ResourceFieldDefinitionDef { get; }
    Dictionary<string, CreatureDefinition> CreaturesDictionary { get; }
    Dictionary<string, Dictionary<int, CreatureLevelDefinition>> CreatursLevelsDictionary { get; }
    Dictionary<string, ResourceDefinition> ResourcesDictionary { get; }
}
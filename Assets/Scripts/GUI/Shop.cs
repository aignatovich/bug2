﻿using UnityEngine;
using UnityEngine.UI;

//ToDo переделать некогда
public class Shop : MonoBehaviour
{
    [SerializeField] private GameObject shopItemPrefab;
    [SerializeField] private HorizontalLayoutGroup horizontalLayout;

    public void Init()
    {
        ShopItem shopItem;

        for ( int i = 0; i <= 25; ++i )
        {
            shopItem = ( GameObject.Instantiate( shopItemPrefab ) as GameObject ).GetComponent<ShopItem>();

            if ( i % 2 == 0 )
            {
//                shopItem.Init( i + "Scout", GameManager.Instance.Definitions.CreaturesDictionary[Constants.ScoutDefId].PassToPrefab );
            }
            else
            {
//                shopItem.Init( i + "Forager", GameManager.Instance.Definitions.CreaturesDictionary[Constants.ForagerDefId].PassToPrefab );
            }

            shopItem.OnItemSelected += OnBuyItem;
            shopItem.transform.SetParent( horizontalLayout.transform );
        }

        ( horizontalLayout.transform as RectTransform ).sizeDelta = new Vector2( horizontalLayout.transform.childCount * ( shopItemPrefab.transform as RectTransform ).rect.width, ( horizontalLayout.transform as RectTransform ).sizeDelta.y );
        LayoutRebuilder.MarkLayoutForRebuild( horizontalLayout.transform as RectTransform );
        
    }

    private void OnBuyItem( object sender, ShopItem.ShopItemEventArgs e )
    {
        Debug.LogError( "buy " + e.PathToPrefab );
    }
}

﻿using System.Collections.Generic;

public class ResourceFieldDefinition : BaseDefinition
{
    public string ResourceFieldPrefabPath { get; private set; }

    public Dictionary<string, float> RateResources { get; private set; }
    public Dictionary<string, float> StartResources { get; private set; }

    public float Radius { get; private set; }

    public ResourceFieldDefinition( string definitionId, string resourceFieldPrefabPath, Dictionary<string, float> startResources, Dictionary<string, float> rateResources, float radius ) : base( definitionId )
    {
        ResourceFieldPrefabPath = resourceFieldPrefabPath;
        StartResources = startResources;
        RateResources = rateResources;
        Radius = radius;
    }
}

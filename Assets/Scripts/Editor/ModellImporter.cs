﻿using UnityEngine;
using UnityEditor;

public class ModellImporter : AssetPostprocessor
{
    [MenuItem( "Custom Functions/Disable shadows in assets" )]
    private static void DisabeShadowsInAssets()
    {
        var guids = AssetDatabase.FindAssets( "t:gameObject", null );

        foreach ( var guid in guids )
        {
            var asssetPath = AssetDatabase.GUIDToAssetPath( guid );
            var asset = AssetDatabase.LoadAssetAtPath( asssetPath, typeof ( GameObject ) ) as GameObject;
            if ( asset != null )
            {
                DisableShadowsInObject( asset );
            }
        }
    }

    private void OnPostprocessModel( GameObject gameObject )
    {
        DisableShadowsInObject( gameObject );

        foreach ( Transform child in gameObject.transform )
        {
            DisableShadowsInObject( child.gameObject );
        }
    }

    private static void DisableShadowsInObject( GameObject gameObject )
    {
        var meshRenderer = gameObject.GetComponent<MeshRenderer>();
        if ( meshRenderer != null )
        {
            Debug.Log( "Deisable shadow in mesh: " + gameObject.name );
            meshRenderer.castShadows = false;
            meshRenderer.receiveShadows = false;
        }

        foreach ( Transform child in gameObject.transform )
        {
            DisableShadowsInObject( child.gameObject );
        }
    }
}

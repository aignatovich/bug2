﻿using UnityEngine;

public class ScoutInfo  
{
	public TypeOfScoutingObject typeOfObject;
	public Vector2 positionOnGrid;

	public ScoutInfo( TypeOfScoutingObject typeOfObject, Vector2 positionOnGrid )
	{
		this.typeOfObject = typeOfObject;
		this.positionOnGrid = positionOnGrid;
	}

	public ScoutInfo( TypeOfScoutingObject typeOfObject, Vector3 positionOnGrid )
	{
		this.typeOfObject = typeOfObject;
		this.positionOnGrid = new Vector2( positionOnGrid.x, positionOnGrid.z );
	}

	public static bool operator == ( ScoutInfo leftScoutInfo, ScoutInfo rightScoutInfo )
	{
		return 	( leftScoutInfo.typeOfObject == rightScoutInfo.typeOfObject ) && 
				( leftScoutInfo.positionOnGrid.x == rightScoutInfo.positionOnGrid.x ) && 
				( leftScoutInfo.positionOnGrid.y == rightScoutInfo.positionOnGrid.y );
	}

	public static bool operator != ( ScoutInfo leftScoutInfo, ScoutInfo rightScoutInfo )
	{
		return 	( leftScoutInfo.typeOfObject != rightScoutInfo.typeOfObject ) || 
				( leftScoutInfo.positionOnGrid.x != rightScoutInfo.positionOnGrid.x ) || 
				( leftScoutInfo.positionOnGrid.y != rightScoutInfo.positionOnGrid.y );
	}

    public override string ToString()
    {
        return string.Format( Constants.ScoutInfoToString, typeOfObject, positionOnGrid ) + base.ToString();
    }
}

public enum TypeOfScoutingObject
{
	Resource,
	Enemy
}

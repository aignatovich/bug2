﻿using UnityEngine;
using Zenject;

public class BaseFsmBehavior : MonoBehaviour
{
    protected FinitStateMachine<BaseFsmBehavior> fsm;
    
    [Inject] protected DiContainer container;

    [PostInject]
    protected virtual void PostInject()
    {
        InitStateMachine();
    }

	void Update () 
    {
	    fsm.Execute();
	}

    protected virtual void InitStateMachine()
    {
        fsm = new FinitStateMachine<BaseFsmBehavior>( this, container );
    }
}

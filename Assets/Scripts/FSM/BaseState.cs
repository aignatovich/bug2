﻿
public class BaseState<TContext> : IState
{
    public  string nextState { get;  set; }

    protected TContext context;

    public BaseState( TContext context )
    {
        this.context = context;
    }

    public virtual void Enter()
    {
    }

    public virtual FSMResult Execute()
    {
        return FSMResult.Continiue;
    }

    public virtual void Exit()
    {
    }
}

public enum FSMResult
{
    Continiue,
    Finished,
    Cancel
}
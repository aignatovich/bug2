﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;

namespace UnityTest
{
    [TestFixture]
    [Category("Definitions Tests")]
    internal class DefinitionsTests
    {
        [Test]
        [Category("Fill definitiion  Test")]
        public void InitFurragerDefinition()
        {
            var testForagerId = "foragerID";
            var testForragerName = "foragerName";
            var testForragerPath = "path";
            var testHp = 10;
            var testVisionDistance = 3.5f;
            var testVisionAngle = 5;
            var testResourceCapacity = 10;
            var testDistanceFromSwarm = 4.5f;
            var foragerDefinition = new ForagerDefinition(testForagerId, testForragerName, testForragerPath, testHp,
                testVisionDistance, testVisionAngle, testResourceCapacity, testDistanceFromSwarm);

            Assert.AreEqual(foragerDefinition.DefinitionId, testForagerId);
            Assert.AreEqual(foragerDefinition.Name, testForragerName);
            Assert.AreEqual(foragerDefinition.PassToPrefab, testForragerPath);
            Assert.AreEqual(foragerDefinition.Hp, testHp);
            Assert.AreEqual(foragerDefinition.VisionDistance, testVisionDistance);
            Assert.AreEqual(foragerDefinition.VisionAngle, testVisionAngle);
            Assert.AreEqual(foragerDefinition.ResourceCapacity, testResourceCapacity);
            Assert.AreEqual(foragerDefinition.MaxDistanceFromSwarm, testDistanceFromSwarm);
        }
    }
}
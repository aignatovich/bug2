﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

// пул пока будет использоваться для хранения GameObject'ов, айдишником будет путь к префабу,
public class GlobalPool : IPool, IDisposable
{
    private GameObject poolGameObject;
    private Dictionary<string, List<GameObject>> gameObjectsPool;

    public GlobalPool()
    {
        gameObjectsPool = new Dictionary<string, List<GameObject>>();
        poolGameObject = new GameObject( "Pool" );
        poolGameObject.transform.position = new Vector3( -1000, -1000, -1000 );
    }

    public T Spawn<T>( string pathToPrefab, Vector3 spawnPosition, Quaternion spawnRotation ) where T : Component
    {
        GameObject spawnedObject = null;

        if( gameObjectsPool.ContainsKey( pathToPrefab ) && gameObjectsPool[pathToPrefab].Count > 0 )
        {
            spawnedObject = gameObjectsPool[pathToPrefab].First();
            gameObjectsPool[pathToPrefab].Remove( spawnedObject );
        }
        else
        {
            spawnedObject = GameObject.Instantiate( Resources.Load<GameObject>( pathToPrefab ) ) as GameObject;
        }

        T spawnedScript = spawnedObject.GetComponent<T>();

        if( spawnedScript == null )
        {
            spawnedScript = spawnedObject.AddComponent<T>();
        }

        var spawnedTransform = spawnedObject.transform;
        spawnedTransform.parent = null;
        spawnedTransform.position = spawnPosition;
        spawnedTransform.rotation = spawnRotation;
        spawnedObject.SetActive( true );

        return spawnedScript;
    }

    public GameObject Spawn( string pathToPrefab )
    {
        GameObject spawnedObject = null;

        if( gameObjectsPool.ContainsKey( pathToPrefab ) && gameObjectsPool[pathToPrefab].Count > 0 )
        {
            spawnedObject = gameObjectsPool[pathToPrefab].First();
            gameObjectsPool[pathToPrefab].Remove( spawnedObject );
        }
        else
        {
            spawnedObject = GameObject.Instantiate( Resources.Load<GameObject>( pathToPrefab ) );
        }

        return spawnedObject;
    }

    public void Despawn( string pathToPrefab, GameObject despawnedObject )
    {
        if( !gameObjectsPool.ContainsKey( pathToPrefab ) )
        {
            gameObjectsPool.Add( pathToPrefab, new List<GameObject>() );
        }

        despawnedObject.SetActive( false );
        despawnedObject.transform.parent = poolGameObject.transform;
        despawnedObject.transform.position = Vector3.zero;

        gameObjectsPool[pathToPrefab].Add( despawnedObject );
    }

    public int CountStoredObjects( string pathToPrefab )
    {
        if( !gameObjectsPool.ContainsKey( pathToPrefab ) )
        {
            return 0;
        }

        return gameObjectsPool[pathToPrefab].Count;
    }

    public int CountAllStoredObjects()
    {
        int countOfObjects = 0;

        foreach ( var list in gameObjectsPool )
        {
            countOfObjects += list.Value.Count;
        }

        return countOfObjects;
    }

    public void Dispose()
    {
        GameObject.DestroyImmediate( poolGameObject );
    }
}

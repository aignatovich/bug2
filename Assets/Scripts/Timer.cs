﻿public class Timer 
{
    float time;

    public bool IsElapsed
    { 
        get 
        {
			return time < 0;
        } 
    }

    public Timer( float time )
    {
        this.time = time;
    }

    public void Elapse( float deltaTime)
    {
        time -= deltaTime;
    }

    public void Reset( float time )
    {
        this.time = time;
    }
}

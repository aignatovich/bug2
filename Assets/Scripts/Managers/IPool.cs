using UnityEngine;

public interface IPool
{
    T Spawn<T>( string pathToPrefab, Vector3 spawnPosition, Quaternion spawnRotation ) where T : Component;
    GameObject Spawn( string pathToPrefab );
    void Despawn( string pathToPrefab, GameObject despawnedObject );

    int CountStoredObjects( string pathToPrefab );
    int CountAllStoredObjects();
}
﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ResourcessField : MonoBehaviour
{
    [Inject] private IDefinitions definitions;
    private Dictionary<string, Timer> seedTimers;

    [PostInject]
    public void Init()
    {
        seedTimers = new Dictionary<string, Timer>();
        foreach (var rateResource in definitions.ResourceFieldDefinitionDef.RateResources)
        {
            seedTimers.Add( rateResource.Key, new Timer( rateResource.Value ) );
        }
    }

    private void Update()
    {
        foreach ( var seedTimer in seedTimers )
        {
            seedTimer.Value.Elapse( Time.deltaTime );
            if ( seedTimer.Value.IsElapsed )
            {
                CreateResource( seedTimer.Key );
                seedTimer.Value.Reset(definitions.ResourceFieldDefinitionDef.RateResources[seedTimer.Key]);
            }
        }
    }

    public void CreateResource( string resourceId )
    {
        var resourcePosition = Utils.GetRandomPointInRange(transform.position, definitions.ResourceFieldDefinitionDef.Radius);
        var newResourceObject = Utils.InstatiateObjectByPass<Transform>(definitions.ResourcesDictionary[resourceId].ResourcePrefabPath, resourcePosition, Quaternion.identity);
        newResourceObject.transform.parent = transform;
    }

    public class Factory : GameObjectFactory<ResourcessField>
    {
    }
}

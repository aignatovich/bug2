﻿public interface IState
{
    string nextState { get;  set; }

    void Enter();
    FSMResult Execute();
    void Exit();
}

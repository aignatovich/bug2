﻿using System;

public class ExtendedFloat
{
    public event EventHandler<ExtendedFloatEventArgs> Changed;

    private float currentValue;

    public float Value
    {
        get { return currentValue; }

        set
        {
            float oldValue = currentValue;
            currentValue = value;
            OnChanged( new ExtendedFloatEventArgs( oldValue, currentValue ) );
        }
    }

    public ExtendedFloat( float value )
    {
        Value = value;
    }

    public static implicit operator ExtendedFloat( float value )
    {
        return new ExtendedFloat( value );
    }

    public static implicit operator float( ExtendedFloat currentInstance )
    {
        return currentInstance.Value;
    }

    protected virtual void OnChanged( ExtendedFloatEventArgs e )
    {
        if ( Changed != null )
        {
            Changed( this, e );
        }
    }


    #region EventArgs

    public class ExtendedFloatEventArgs : EventArgs
    {
        public float OldValue { get; private set; }
        public float NewValue { get; private set; }

        public ExtendedFloatEventArgs( float oldValue, float newValue )
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }

    #endregion
}
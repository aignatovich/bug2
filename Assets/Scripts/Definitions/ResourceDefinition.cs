﻿public class ResourceDefinition : BaseDefinition
{
    public string ResourcePrefabPath { get; private set; }

    public ResourceDefinition( string definitionId, string resourcePrefabPath ) : base( definitionId )
    {
        ResourcePrefabPath = resourcePrefabPath;
    }
}

﻿using UnityEngine;
using Zenject;

public class SearchForResourcesForagerState : BaseState<Forager>
{
    [Inject] private ISwarmManager swarmManager;
    [Inject] private IGameModel gameModel;
    [Inject] private IDefinitions definitions;

    public SearchForResourcesForagerState( Forager forager ) : base( forager )
    {
    }

    public override FSMResult Execute()
    {
        if ( Vector3.Distance( context.transform.position, swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position ) > ( definitions.CreaturesDictionary[gameModel.CreaturesDictionary[context.ModelId].DefinitionId] as ForagerDefinition ).MaxDistanceFromSwarm )
        {
            TurnNearer();
        }

        context.Step( Vector3.forward );
        if ( TryFindResource() )
        {
            nextState = FsmNames.TryCaptureResourcessForagerState.ToString();
            return FSMResult.Finished;
        }

        return FSMResult.Continiue;
    }

    private void TurnNearer()
    {
        var angle = Vector3.Angle( swarmManager.Swarms[gameModel.CreaturesDictionary[context.ModelId].SwarmModelId].Position - context.transform.position, Vector3.forward ) + Random.Range( -Constants.AngleForTurn / 2, Constants.AngleForTurn / 2 );
        context.transform.Rotate( Vector3.up, angle );
    }

    private bool TryFindResource()
    {
        GameObject findedObject;
        if ( RaycastUtils.RaycastByAngle( context.transform, definitions.CreaturesDictionary[gameModel.CreaturesDictionary[context.ModelId].DefinitionId].VisionDistance, definitions.CreaturesDictionary[gameModel.CreaturesDictionary[context.ModelId].DefinitionId].VisionAngle, 1 << LayerMask.NameToLayer( Layers.Resource.ToString() ), out findedObject ) )
        {
            context.transform.LookAt( findedObject.transform.position );
            return true;
        }

        return false;
    }
}

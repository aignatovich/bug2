﻿using UnityEngine;

public static class Utils
{
    public static Vector3 GetRandomPointInRange( Vector3 center, float range )
    {
        float randomDistance = Random.Range( -range, range );
        float randomAngle = Random.Range( 0, 360 );

        float newZ = Mathf.Cos( randomAngle ) * randomDistance + center.z;
        float newX = Mathf.Sin( randomAngle ) * randomDistance + center.x;

        return new Vector3( newX, center.y, newZ );
    }

    public static Vector2 GetRandomPointInRect( Rect rect )
    {
        var x = Random.Range( rect.xMin, rect.xMax );
        var y = Random.Range( rect.yMin, rect.yMax );

        return new Vector2( x, y );
    }

    public static T SpawnObjectInRandomPosition<T>( GameObject prefab, Rect spawnArea ) where T : Component
    {
        var spawnPoint = GetRandomPointInRect( spawnArea );
        var instatiatedObject = GameObject.Instantiate( prefab, new Vector3( spawnPoint.x, 0, spawnPoint.y ), Quaternion.identity ) as GameObject;
        return instatiatedObject.GetComponent<T>();
    }

    public static T InstatiateObjectByPass<T>( string pass ) where T : Component
    {
        var prefab = Resources.Load( pass );
        if ( prefab == null )
        {
            Debug.LogError( "Utils.InstatiateObjectByPass prefab by pass " + pass + " is NULL" );
            return null;
        }

        var script = ( GameObject.Instantiate( prefab, Vector3.zero, Quaternion.identity ) as GameObject ).GetComponent<T>();

        if ( script == null )
        {
            Debug.LogError( "Utils.InstatiateObjectByPass prefab by pass " + pass + " requred script " + typeof ( T ).Name + " is null" );
        }

        return script;
    }

    public static T InstatiateObjectByPass<T>( string pass, Vector3 position, Quaternion rotation, GameObject parent = null ) where T : Component
    {
        var instatiatedObject = InstatiateObjectByPass<T>( pass );
        instatiatedObject.transform.position = position;
        instatiatedObject.transform.rotation = rotation;
        if ( parent != null )
        {
            instatiatedObject.transform.SetParent( parent.transform );
        }

        return instatiatedObject;
    }

    public static T InstatiateObjectByPass<T>( string pass, GameObject parent ) where T : Component
    {
        var instatiatedObject = InstatiateObjectByPass<T>( pass );
        if ( parent == null )
        {
            Debug.LogWarning( "Utils.InstatiateObjectByPass parent is NULL" );
        }
        else
        {
            instatiatedObject.transform.SetParent( parent.transform );
        }

        instatiatedObject.transform.localPosition = Vector3.zero;
        instatiatedObject.transform.localRotation = Quaternion.identity;
        return instatiatedObject;
    }
}
